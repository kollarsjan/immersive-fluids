# Immersive fluids

This project provides a proof of concept of the integration of multiple devices in the usage of a website.

## Setup

Make sure node and npm are installed in a recent version.

The dependencies can then be installed like this:

```shell
npm install
```
or
```shell
yarn install
```

### Building the project and starting the server

```shell
npm run dev
```
or
```shell
yarn dev
```

## How to use it

In addition to starting the server as described above, you need to make sure that the server is accessible to other devices in the first place. A setup in the local network is recommended in this regard. You may want to fire up a terminal and execute `ifconfig` or `ipconfig` (Windows). This will show you your ip address.  
Open a browser (preferably Chromium-based or Firefox) and open `https://[your ip here]:8443`. Accept the warning which is caused by the self-signed certificate. The website also works over the unencrypted connection `http://[your ip here]:8080`. However, the motion-sensor-controlled option will not be presented to you as the sensor api requires ssl.

You will be presented with a randomly generated qr code. It contains the url with information about the current session. Scan it with the other device which should be the controlling device. Again a recent browser is preferred. The current state of the browser support of the generic sensor api can be found on [Can I use](https://caniuse.com/#feat=mdn-api_sensor). Once the website is loaded on the controlling device you can select the available methods of interaction. The touch option will always be available. The second option, however, will only be available if the device believes that it can access the sensor.

- touch: Touch your screen it will receive the inputs of all your fingers. The corners of the screens will be matched so you can use devices regardless of their aspect ratios. This is done by converting the inputs into relative values.
- motion: Before clicking the button, holding the phone in a comfortable position pointing at the monitoring device is advised.

> Pro tip: If you have a VR headset try to use it as the monitoring device. It will also look very cool on projectors and large screens.

Enjoy!

## Explanation of the project

The project is web-based and mainly written in JavaScript. The dependencies, as well as the code, can be divided into two categories. Server code can be found in the `server.js`. The frontend code is scattered across `public/`, `src/` and `store/`.  
The server uses the `Express.js` framework and `socket.io`.  
The frontend is a `Svelte` SPA. The HTML-file is located in `public/`. The actual application code can be found in `src/`. The sessionID and the socket connection need to be available across the Svelte-components. This is why the application uses a store. The frontend code is built using the bundler [Rollup](https://rollupjs.org/). All configuration files are in the root directory.

## My dev experience

I wanted to develop an immersive experience using web technologies. In the beginning, I considered creating a simple yet VR-enabled version of the [lightsaber escape game](https://experiments.withgoogle.com/lightsaber-escape) by the Chrome Dev Team. I spent hours reading the documentation of the WebXR API and examining examples for web-based VR. I also joined a Meetup by Mozilla which featured a talk about the API. After my research, I decided to discard my initial idea because of my lack of experience concerning WebGL and WebRTC (which was another technology I originally wanted to use) and because of the early state of WebXR. However, the idea of using multiple devices as part of the usage of a website was fascinating to me. Luckily, I discovered an interesting project by [PavelDoGreat](https://github.com/PavelDoGreat/) on Twitter. The [WebGL Fluid Simulation](https://github.com/PavelDoGreat/WebGL-Fluid-Simulation) appeared to be an ideal candidate for the implementation of a multi-device setup. I had already started developing the multi-device communication. Unfortunately, programming a WebRTC application turned out unreasonably hard. The standard was released in 2011. Most articles and documentation were written around 2013 and appeared to be outdated or experimental. After many unsuccessful attempts, I decided to use the more solid WebSockets (specifically the popular and minimalistic package [ws](https://www.npmjs.com/package/ws)). This was a very pleasant experience. I finally managed to establish a communication channel between my laptop and my phone over a website. Nevertheless, I replaced it in favour of [socket.io](https://www.npmjs.com/package/socket.io) because of its ease of implementation when it comes to multi-channel (or "rooms") communication. Likewise, the webserver was refactored using [Express](https://www.npmjs.com/package/express) which enabled the easy configuration of a certificate which I generated with OpenSSL.

The frontend stack was quite exciting because I wanted to give [Svelte](https://svelte.dev/) a try. It is celebrated among the frontend community for being a completely different framework which compiles your code into plain javascript with watchers which take care of the reactivity - no VDOM is used. After reading the docs and playing around, Svelte became very intuitive although there are certain things which remain unclear to me. To give you an idea, global styles in a component will take effect even though it is not loaded. Although I am used to Webpack and task runners such as Gulp, I tried Rollup because it is promoted by Svelte. The Rollup-config was generated by Svelte. The bundler worked reliably in general.

### My verdict and final thoughts

WebRTC and WebXR are exciting technologies. However, they are not very approachable. I will keep an eye on resources concerning the two topics and will resume the project once I have the required skills.

Being able to control a website remotely and immersively really empowers further development in various fields. A colleague pointed out that both the touch and the motion gestures are well suited for controlling a presentation. I like the idea a lot and will definitely combine my controlling mechanism with reveal.js and impress.js presentations.

const fs = require('fs');
const express = require('express')
const app = express()

let options = {}
if (process.env.NODE_ENV !== 'production') {
  options = {
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
  }
}

const https = require('https').createServer(options, app)
const http = require('http').createServer(app)

const io = require('socket.io')()

app.use(express.static('./public'))

https.listen(8443, () => {
  console.log('Webserver is running port 8443')
})
http.listen(8080, () => {
  console.log('Webserver is running port 8080')
})

io.attach(https)
io.attach(http)

io.on('connection', function (socket) {
  console.log('a user connected')

  socket.on('message', function (text) {
    console.log(text)
    io.emit('message', text)
  })

  socket.on('transmissionstatus', function ({sessionID, text}) {
    console.log(sessionID, text)
    socket.to(sessionID).emit('transmissionstatus', text)
  })

  socket.on('coordinates', function (data) {
    socket.to(data.sessionID).emit('coordinates', data)
  })

  socket.on('join', function (room) {
    socket.join(room)
    console.log('joined room ' + room)
  })

  socket.on('disconnect', function () {
    console.log('user disconnected')
  })
})

console.log('Websocket Server ready')

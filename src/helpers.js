export const demandFullScreen = () => {
  const docElm = document.documentElement
  if (docElm.requestFullscreen) {
    docElm.requestFullscreen()
  } else if (docElm.mozRequestFullScreen) {
    docElm.mozRequestFullScreen()
  } else if (docElm.webkitRequestFullScreen) {
    docElm.webkitRequestFullScreen()
  } else if (docElm.msRequestFullscreen) {
    docElm.msRequestFullscreen()
  }
}

export const calculateRelativeCoordinates = (posX, posY, deviceX, deviceY) => {
  let x = posX / deviceX
  let y = posY / deviceY
  if (x > 1) x = 1
  if (y > 1) y = 1
  return { x: x, y: y }
}

export const getDeviceDimensions = () => {
  return {
    x: Math.max(
      document.documentElement.clientWidth,
      window.innerWidth || 0
    ),
    y: Math.max(
      document.documentElement.clientHeight,
      window.innerHeight || 0
    )
  }
}
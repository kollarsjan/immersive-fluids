import { writable } from 'svelte/store'
import io from 'socket.io-client'

export const sessionID = writable(null)

export const webSocket = writable(io(`${window.location.protocol}//${window.location.host}`))
